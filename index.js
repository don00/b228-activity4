//naming convention for making classes (Capital first letter)

class Student{
	// creating a constructor

	constructor({name, email, grades}){
		this.name = name;
		this.email = email;
		this.grades = (grades.every(grade => (grade >= 0 && grade <= 100)) && grades.length === 4)? grades : undefined;
    }

	login() {
		console.log(`${this.email} is logged in`);
		return this;
	}

	logout(){
		console.log(`${this.email} is logged out`);
		return this;
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
		return this;
	}

	computeAve(){
        this.gradeAve = this.grades.reduce((a,b) => a + b) / this.grades.length;    
		return this;
	}
	willPass() {
	    this.passed = this.gradeAve >= 85;
	    return this;
	}
	willPassWithHonors() {

	    if(this.passed){
            this.passedWithHonors = this.gradeAve >= 90;
	    } else {
	    	this.passedWithHonors = undefined;
	    }
	    return this;
	}

}

class Section {
    constructor(name){
        this.name = name;
        this.students = [];
        this.honorsPercentage = undefined;
        this.honorStudents = undefined;
    }

    addStudent({name, email, grades}){
        this.students.push(new Student({name, email, grades}));
        return this;
    }

    countHonorStudents(){

        // this.students.forEach(student => student.computeAve().willPass().willPassWithHonors());
        // const withHonors =  this.students.filter(student => student.passedWithHonors);
        // this.honorStudents = withHonors.length;
        let count = 0;
        this.students.forEach(student => {
            if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
                count++;
            }
        })

        this.honorStudents = count;
        return this;
    }

    // create a method that is a replica of computeHonorsPercentage()
    computeHonorsPercentage(){
        this.honorsPercentage = (this.honorStudents / 4) * 100;
        // console.log(this);
        return this;
    }
}

/* 
    1. Define a Grade class whose constructor will accept a number argument to serve as its grade level. It will have the following properties:
    level initialized to passed in number argument
    sections initialized to an empty array
    totalStudents initialized to zero
    totalHonorStudents initialized to zero
    batchAveGrade set to undefined
    batchMinGrade set to undefined
    batchMaxGrade set to undefined
*/
class Grade{
    constructor(gradeLevel){
        this.gradeLevel  = gradeLevel;
        this.sections = [];
        this.totalStudents = 0;
        this.totalHonorStudents = 0;
        this.batchAveGrade = undefined;
        this.batchMinGrade = undefined;
        this.batchMaxGrade = undefined;
    }

    // 2.Define an addSection() method that will take in a string argument to instantiate a new Section object and push it into the sections array property.
    addSection(name){
        this.sections.push(new Section(name));
        return this;
    }

    // 4. Define a countStudents() method that will iterate over every section in the grade level, incrementing the totalStudents property of the grade level object for every student found in every section.
    countStudents(){
        for(let section of this.sections){
            for(let student of section.students){
                this.totalStudents++;
            };            
        };
        return this;
    }

    // 5. Define a countHonorStudents() method that will perform similarly to countStudents() except that it will only consider honor students when incrementing the totalHonorStudents property.
    countHonorStudents(){
        for(let section of this.sections){
            for(let student of section.students){
                student.computeAve().willPass().willPassWithHonors();
                student.passedWithHonors?  this.totalHonorStudents++: this.totalHonorStudents
            };            
        };
        return this;
    }

    // 6. Define a computeBatchAve() method that will get the average of all the students' grade averages and divide it by the total number of students in the grade level. The batchAveGrade property will be updated with the result of this operation.
    computeBatchAve(){
        countStudents();
        let totalGrades = 0;

        for(let section of this.sections){
            for(let student of section.students){
                totalGrades += student.computeAve().gradeAve;    
            };            
        };
        this.batchAveGrade = totalGrades /  this.totalStudents;
        return this;
    }

    // 7. Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade scored by a student of this grade level regardless of section.
    getBatchMinGrade(){
        this.batchMinGrade = 100;
        for(let section of this.sections){
            for(let student of section.students){
                for(let grade of student.grades)
                if(grade < this.batchMinGrade) this.batchMinGrade = grade;
            };            
        };
        return this;
    }

    // 8. Define a method named getBatchMaxGrade() that will update the batchMaxGrade property with the highest grade scored by a student of this grade level regardless of section.

    getBatchMaxGrade(){
        this.batchMaxGrade = 0;
        for(let section of this.sections){
            for(let student of section.students){
                for(let grade of student.grades)
                if(grade > this.batchMaxGrade) this.batchMaxGrade = grade;
            };            
        };
        return this;
    }

}

// 3.Write a statement that will add a student to section1A. The student is named John, with email john@mail.com, and grades of 89, 84, 78, and 88.
const grade1 = new Grade(1);
grade1.addSection("section1A");

grade1.sections.find(section => section.name === "section1A").addStudent({email: "john@email.com", name: "john", grades: [89, 84, 78, 88]});

grade1.sections.find(section => section.name === "section1A").addStudent({email: "joe@email.com", name: "joe", grades: [72, 82, 79, 85]});
grade1.sections.find(section => section.name === "section1A").addStudent({email: "jane@email.com", name: "jane", grades: [87, 89, 91, 93]});
grade1.sections.find(section => section.name === "section1A").addStudent({email: "jessie@email.com", name: "jessie", grades: [91, 89, 92, 93]});
